# Antistasi: An Arma III Guerrilla simulator
- This is NOT the stable version available through Steam or A3Antistasi.com
- Use this version at your own discretion, do not expect it to be thoroughly tested
- An Altis SQM is included, additional SQMs and world-specific files are available in the other repository

## WAFFL Fork
WAFFL private fork of http://www.a3antistasi.com/mod

### Changelog
Last merge: 1.7.5-BETA 2017-04-24.

###### WAFFL Changes:
* Unlocking items no longer increases the number of items required to unlock the next.
* New static unlock requirements are as follows:
 * **Weapons** `15 - 1/Factory`
 * **Backpacks** `15 - 1/Factory`
 * **Vests** `15 - 1/Factory`
 * **Magazines** `25 - 2/Factory`
 * **Items** `25 - 2/Factory`
 * **Default** `25 - 2/Factory`
* "Imported Items" Has been renamed "Unrecognized Items" to make it clearer what they are.
* The following default parameters have been changed:
 * **Launchers** - `On`
 * **How much damage can an engineer repair?** - `Half`
 * **Explosives explode on defusal?** - `On`
 * **Wheel repair requires toolkit** - `None`